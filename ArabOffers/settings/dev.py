"""
Django settings for ArabOffers project.

Generated by 'django-admin startproject' using Django 1.9.7.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.9/ref/settings/
"""
from ArabOffers.settings.common import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'offersdb',
        'USER': 'offersuser',
        'PASSWORD':'offersuser',
        'HOST':'',
        'PORT':'',
    }
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'assets')

STATIC_URL = '/static/'

STATICFILES_DIRS =  [os.path.join(PROJECT_ROOT, 'static')]

PUSH_NOTIFICATIONS_SETTINGS = {
        "GCM_API_KEY": "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0R1BWdoEJzbD18YZdBZAEcPxTp4J8KAeNJk/W2RW8wzbn4PHizHwI8XK55Qtc1VnZw76G2k7x5mJ24NgbcDWWryeZlYf1x6YMPyfgTwmpAGE9k8mlHLBniaMIygDcIbGjrLwKiAg7kP6X3NsNoyAmgfF9COG35Q8tUXYvoFu/oq/dVNzXbE4+KYw4wGjJPaKj+paNrfMUzo0gQ1pT52vopfm/Zpq93Ds+/49LR2PVGKTUYGAwTzTtOzy6d8btGGNE3VIkY6qByo6KMXqEghwH8KCoSwvqW5CS5bGJdAX/ellKeLw00uNc1oU10aYN2XVWHPqvbopOV57eEsByqXxmwIDAQAB",
        "APNS_CERTIFICATE": os.path.join(STATICFILES_DIRS[0], 'push_cert/Offers_Dev_APNS.pem'),
        "USER_MODEL":"offers.AppUser",
        }
