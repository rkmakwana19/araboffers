"""ArabOffers URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from offers import views
from offers.views import UserList, UserDetail, OfferList, OfferDetail, CommentDetail, AppIdsDetails, LikeCreate, AjaxChainedCities, AjaxChainedRegions
from areas.views import CountryList, CountryDetails, CityDetails, RegionDetails

admin.autodiscover()

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^admin/send_notification/$', views.notif, name='notif'),
    url(r'^admin/offers/send_notification/$', views.notif, name='notif'),
    url(r'^chaining/', include('smart_selects.urls')),
    url(r'^ajax/chained-cities/$', AjaxChainedCities.as_view(), name='ajax_chained_cities'),
    url(r'^ajax/chained-regions/$', AjaxChainedRegions.as_view(), name='ajax_chained_regions'),
    url(r'^users/$', UserList.as_view()),
    url(r'^users/(?P<pk>[0-9]+)/$', UserDetail.as_view()),
    url(r'^users/settings/(?P<pk>[0-9]+)/$', views.update_settings, name='update_settings_api'),
    url(r'^appsettings/$', AppIdsDetails.as_view()),
    url(r'^country/$', CountryList.as_view()),
    url(r'^country/(\w+)/', CountryDetails.as_view()),
    url(r'^region/(\d+)/', RegionDetails.as_view()),
    url(r'^city/(\d+)/', CityDetails.as_view()),
    url(r'^offers/$', OfferList.as_view()),
    url(r'^offers/user=(?P<pk>[0-9]+)/$', OfferList.as_view()),
    url(r'^offers/(?P<pk>[0-9]+)/$', OfferDetail.as_view()),
    url(r'^comments/(?P<pk>[0-9]+)/$', CommentDetail.as_view(), name='comment_list'),
    url(r'^comments/$', CommentDetail.as_view(), name='comment_creation'),
    url(r'^like/$', LikeCreate.as_view()),
]
