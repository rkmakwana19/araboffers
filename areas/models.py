# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models


# Create your models here.
class Country(models.Model):
    class Meta:
        verbose_name = 'Country'
        verbose_name_plural = 'Countries'
    name = models.CharField(max_length=35)
    english_name = models.CharField(max_length=35)
    phone_code = models.CharField(max_length=5)

    def cities(self):
        return self.city_set.count()

    def __str__(self):
        return self.name

    def __unicode__(self):
        return u'%s' % (self.name)

class City(models.Model):
    class Meta:
        verbose_name = 'City'
        verbose_name_plural = 'Cities'
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    name = models.CharField(max_length=40)
    english_name = models.CharField(max_length=35, null=True, blank=True)

    def regions(self):
        return self.region_set.count()

    def country_name(self):
        return self.country.name

    def __str__(self):
        return self.name

    def __unicode__(self):
        return u'%s' % (self.name)

class Region(models.Model):
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    english_name = models.CharField(max_length=35, null=True, blank=True)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return u'%s' % (self.name)
