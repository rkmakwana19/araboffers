# -*- coding: utf-8 -*-
from django.contrib import admin

from django.utils.translation import ugettext_lazy as _

from .models import Country, City, Region


class RegionInline(admin.TabularInline):
    model = Region
    extra = 1

class CityInline(admin.TabularInline):
    model = City
    extra = 1

class CountryAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields':['name', 'english_name', 'phone_code']}),
    ]
    inlines = [CityInline]
    list_display = ('name', 'english_name', 'phone_code', 'cities',)

# Import-Export

from import_export.admin import ImportExportModelAdmin, ImportExportMixin

from import_export import resources, widgets, fields


class CityResource(resources.ModelResource):
    country = fields.Field(column_name='country', attribute='country', widget=widgets.ForeignKeyWidget(Country, 'name'))
    class Meta:
        model = City

class RegionResource(resources.ModelResource):
    city = fields.Field(column_name='city', attribute='city', widget=widgets.ForeignKeyWidget(City, 'name'))
    class Meta:
        model = Region


class CityAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = CityResource
    list_display = ('name', 'english_name', 'country')
    list_filter = ('country',)

class RegionAdmin(ImportExportModelAdmin):
    resource_class = RegionResource
    list_display = ('name', 'english_name', 'city')
    list_filter = ('city',)



admin.site.register(Country, CountryAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Region, RegionAdmin)
