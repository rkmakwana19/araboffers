from django.shortcuts import render
from django.http import Http404

from areas.serializers import CountrySerializer, CitySerializer, RegionSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .models import Country, City, Region
# Create your views here.


class CountryList(APIView):

    def get(self, request, format=None):
        countries = Country.objects.all()
        serializer = CountrySerializer(countries, many=True)
        return Response(serializer.data)

class CountryDetails(APIView):
    def get_object(self, search_key):
        try:
            return Country.objects.filter(name__istartswith=search_key)
        except Country.DoesNotExist:
            raise Http404

    def get(self, request, search_key, format=None):
        country = self.get_object(search_key)
        serializer = CountrySerializer(country, many=True)
        return Response(serializer.data)


class CityDetails(APIView):
    def get_object(self, search_key):
        try:
            c = City.objects.filter(country=(Country.objects.get(id=search_key)))
            return c
        except City.DoesNotExist:
            raise Http404

    def get(self, request, search_key, format=None):
        city = self.get_object(search_key)
        serializer = CitySerializer(city, many=True)
        return Response(serializer.data)


class RegionDetails(APIView):
    def get_object(self, search_key):
        try:
            c = Region.objects.filter(city=(City.objects.get(id=search_key)))
            return c
        except Region.DoesNotExist:
            raise Http404

    def get(self, request, search_key, format=None):
        region = self.get_object(search_key)
        serializer = RegionSerializer(region, many=True)
        return Response(serializer.data)
