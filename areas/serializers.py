from .models import Country, City, Region

from rest_framework import serializers

class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ('id', 'name', 'phone_code', 'english_name',)

class CitySerializer(serializers.ModelSerializer):
    country = CountrySerializer(many=False)
    class Meta:
        model = City
        fields = ('id', 'name', 'english_name', 'country', )

class RegionSerializer(serializers.ModelSerializer):
    city = CitySerializer(many=False)
    class Meta:
        model = Region
        fields = ('id', 'name', 'english_name', 'city', )
