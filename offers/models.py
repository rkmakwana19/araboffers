# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.files.storage import default_storage as storage
import datetime
from django.db import models
from django.utils import timezone
from areas.models import Country, City, Region
from smart_selects.db_fields import ChainedForeignKey
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.contrib.staticfiles.storage import staticfiles_storage
from push_notifications.models import APNSDevice, GCMDevice
import uuid


# Create your models here.
class AppUser(models.Model):
    name = models.CharField(max_length=100)
    mobile = models.CharField(max_length=20)
    AGE_RANGES = (
    ("1", "16-20"),
    ("2", "21-25"),
    ("3", "26-30"),
    ("4", "31-35"),
    ("5", "36-40"),
    ("6", "41-45"),
    ("7", "46-50"),
    ("8", "51-55"),
    ("9", "56-60"),
    ("10","60+"),
    )
    age = models.CharField(max_length=2, choices=AGE_RANGES, default="3")
    GENDER_CHOICE = (
    ("1", "M"),
    ("2", "F"),
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICE)
    country = models.ForeignKey(Country)
    device_token = models.CharField(max_length=20000, null=True, blank=True)
    DEVICE_TYPES = (
    ("1", "iOS"),
    ("2", "Android"),
    ("3", "Other"),
    )
    device_type = models.CharField(max_length=1, choices=DEVICE_TYPES, default="3")
    city = ChainedForeignKey(
        City,
        chained_field="country",
        chained_model_field="country",
        show_all=False,
        auto_choose=True
    )
    region = ChainedForeignKey(Region, chained_field="city", chained_model_field="city")
    created_on = models.DateTimeField('Created_on', auto_now_add=True)

    notif_all = models.BooleanField(default=True)
    notif_supermarket = models.BooleanField(default=True)
    notif_collections = models.BooleanField(default=True)
    notif_restaurants = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    def total_users(self):
        return self.count()

    def __unicode__(self):
        return u'%s' % (self.name)

    def save(self, *args, **kwargs):
        pn_device = None
        exists = False
        dev_type = self.device_type
        i = uuid.UUID(int=int(self.mobile))
        if dev_type == "1":
            pn_device = APNSDevice(registration_id=self.device_token, name=self.mobile)
            exists = APNSDevice.objects.filter(registration_id=self.device_token).exists()
            if exists:
                instance = APNSDevice.objects.get(registration_id=self.device_token)
                instance.delete()
        if dev_type == "2":
            pn_device = GCMDevice(registration_id=self.device_token, name=self.mobile, cloud_message_type="FCM")
            exists = GCMDevice.objects.filter(registration_id=self.device_token).exists()
            if exists:
                instance = GCMDevice.objects.get(registration_id=self.device_token)
                instance.delete()
        pn_device.save()
        super(AppUser, self).save(*args, **kwargs)


class Offer(models.Model):
    name = models.CharField(max_length=200)
    publish_date = models.DateTimeField('Published On', auto_now_add=True)
    start_date = models.DateTimeField('Starts On')
    end_date = models.DateTimeField('Expires On')
    views = models.IntegerField(default=0)
    description = models.TextField(max_length=10000)
    comments_enabled = models.BooleanField(default=True)

    GENDER_CHOICE = (
    ("1", "M"),
    ("2", "F"),
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICE, null=True, blank=True)

    OFFER_TYPE = (
    ("1", "SUPERMARKET"),
    ("2", "RESTAURANT"),
    ("3", "COLLECTION"),
    )
    offer_type = models.CharField(max_length=1, choices=OFFER_TYPE, default="1")

    DEVICE_TYPES = (
    ("1", "iOS"),
    ("2", "Android"),
    ("3", "Other"),
    )
    device_type = models.CharField(max_length=1, choices=DEVICE_TYPES, null=True, blank=True)

    special_offer = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def all_comments(self):
        total_comments = self.comment_set
        return total_comments

    def comments(self):
        total_comments = self.comment_set.count()
        return total_comments

    def likes(self):
        return self.offerlike_set.count()

    def unique_views(self):
        return self.offerview_set.count()

    def has_liked(self, user_id):
        cats = OfferLike.objects.filter(offer__id=self.id , user__id=user_id)
        return cats.count()

    def is_comments_enabled(self):
        return self.comments_enabled

    def images_count(self):
        return self.offerimage_set.count()

    def all_images(self):
        return self.offerimage_set

    def age_ranges(self):
        a=self.agerange_set.all()
        if a.count():
            b=""
            for val in a:
                b += "%s, " %(val.age_value())
            return b[:-2]
        else:
            return "All"

    def in_country(self):
        a=self.offerarea_set.all()
        if a.count():
            b=[]
            for val in a:
                c = "%s" %(val.for_country())
                if c!="None":
                    b.append(c)
            b=list(set(b))
            if len(b):
                return ' ,'.join(b)
            else:
                return "All"
        else:
            return "All"

    def in_city(self):
        a=self.offerarea_set.all()
        if a.count():
            b=[]
            for val in a:
                c = "%s" %(val.for_city())
                if c!="None":
                    b.append(c)
            b=list(set(b))
            if len(b):
                return ' ,'.join(b)
            else:
                return "All"
        else:
            return "All"

    def in_region(self):
        a=self.offerarea_set.all()
        if a.count():
            b=[]
            for val in a:
                c = "%s" %(val.for_region())
                if c!="None":
                    b.append(c)
            b=list(set(b))
            if len(b):
                return ' ,'.join(b)
            else:
                return "All"

        else:
            return "All"

class AgeRange(models.Model):
    AGE_RANGES = (
    ("1", "16-20"),
    ("2", "21-25"),
    ("3", "26-30"),
    ("4", "31-35"),
    ("5", "36-40"),
    ("6", "41-45"),
    ("7", "46-50"),
    ("8", "51-55"),
    ("9", "56-60"),
    ("10","60+"),
    )
    age = models.CharField(max_length=2, choices=AGE_RANGES, blank=True)
    offer = models.ForeignKey(Offer, on_delete=models.CASCADE)

    def age_value(self):
        AGE_RANGES = (
        ("1", "16-20"),
        ("2", "21-25"),
        ("3", "26-30"),
        ("4", "31-35"),
        ("5", "36-40"),
        ("6", "41-45"),
        ("7", "46-50"),
        ("8", "51-55"),
        ("9", "56-60"),
        ("10","60+"),
        )
        return str(dict(AGE_RANGES).get(self.age))

class OfferArea(models.Model):
    offer = models.ForeignKey(Offer, on_delete=models.CASCADE)
    country = models.ForeignKey(Country, null=True, blank=True)
    city = ChainedForeignKey(
        City,
        chained_field="country",
        chained_model_field="country",
        show_all=False,
        auto_choose=True,
        null=True,
        blank=True
    )
    region = ChainedForeignKey(Region, chained_field="city", chained_model_field="city", null=True, blank=True)

    def for_country(self):
        if (self.country is not None):
            return str(self.country.name)

    def for_city(self):
        if (self.city is not None):
            return str(self.city.name)

    def for_region(self):
        if (self.region is not None):
            return str(self.region.name)


class OfferImage(models.Model):
    image = models.ImageField()
    offer = models.ForeignKey(Offer, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.image.url)

class Comment(models.Model):
    offer = models.ForeignKey(Offer, on_delete=models.CASCADE)
    user = models.ForeignKey(AppUser, on_delete=models.CASCADE)
    text = models.CharField(max_length=500)
    time = models.DateTimeField('Date', auto_now_add=True)

    def __str__(self):
        return ''

class OfferLike(models.Model):
    offer = models.ForeignKey(Offer, on_delete=models.CASCADE)
    user = models.ForeignKey(AppUser, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.name

class OfferView(models.Model):
    offer = models.ForeignKey(Offer, on_delete=models.CASCADE)
    user = models.ForeignKey(AppUser, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.name


# APP SETTINGS

class AppSetting(models.Model):
    admob_android = models.CharField(max_length=200)
    admob_ios = models.CharField(max_length=200)
    admin_email = models.CharField(max_length=100)
    share_message = models.CharField(max_length=500)
