from django import forms
from areas.models import Country, City, Region
from django.core.urlresolvers import reverse_lazy
from django.forms import ChoiceField, ModelChoiceField
from django.utils.translation import ugettext_lazy as _

from clever_selects.form_fields import ChainedChoiceField
from clever_selects.forms import ChainedChoicesForm


class NotifForm(ChainedChoicesForm):
    message = forms.CharField(widget=forms.Textarea)
    CHOICES=[('ios','iOS'),
             ('android','Android'),
             ('all','All')]

    group = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect(), initial='all')

    AGE_RANGES = (
    ("0", "All"),
    ("1", "16-20"),
    ("2", "21-25"),
    ("3", "26-30"),
    ("4", "31-35"),
    ("5", "36-40"),
    ("6", "41-45"),
    ("7", "46-50"),
    ("8", "51-55"),
    ("9", "56-60"),
    ("10","60+"),
    )
    age = forms.ChoiceField(choices=AGE_RANGES, initial="0")

    GENDER_CHOICES=[('1','Male'),
             ('2','Female'),
             ('3','All')]

    gender = forms.ChoiceField(choices=GENDER_CHOICES, widget=forms.RadioSelect(), initial='3')

    OFFER_CHOICES=[
             ('0','ALL'),
             ('1','SUPERMARKET'),
             ('2','RESTAURANT'),
             ('3','COLLECTION')]

    offer_choice = forms.ChoiceField(choices=OFFER_CHOICES, widget=forms.RadioSelect(), initial='0')

    for_offer = forms.CharField(max_length=10, required=False)

    allcountry = set([o for o in Country.objects.all()])
    opt = [(o.id, o.name) for o in allcountry]
    country = ChoiceField(choices=[('', _(u'Select a country'))] + opt, required=False)
    city = ChainedChoiceField(parent_field='country', ajax_url=reverse_lazy('ajax_chained_cities'), required=False)
    region = ChainedChoiceField(parent_field='city', ajax_url=reverse_lazy('ajax_chained_regions'), required=False)




    # region = forms.ChoiceField()
