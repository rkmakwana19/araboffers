from .models import Offer
from django.utils import timezone
from django.db.models import Q
from django_cron import CronJobBase, Schedule
from django.core import management

class OffersCronJob(CronJobBase):
    RUN_EVERY_MINS = 1 # every 2 hours

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'offers.remove_expired_offers_job'    # a unique code

    def do(self):
        today = timezone.now()
        print('RUNNING **** THE **** CRON *** JOB')
        offers = Offer.objects.filter(Q(end_date__lte=today))
        for offer in offers:
            offer.delete()
        return 'Cron job success'

class Backup(CronJobBase):
    RUN_AT_TIMES = ['6:00']
    schedule = Schedule(run_at_times=RUN_AT_TIMES)
    code = 'offers_app.Backup'

    def do(self):
        management.call_command('dbbackup')
