# -*- coding: utf-8 -*-

from django.contrib import admin

from django.utils.translation import ugettext_lazy as _

from .models import AppUser, Offer, Comment, OfferImage, AgeRange, OfferArea, AppSetting

from admin_views.admin import AdminViews
from django.contrib.auth.models import *


# Register your models here.

class CommentInline(admin.TabularInline):
    model = Comment
    extra = 1

class OfferImageInline(admin.StackedInline):
    model = OfferImage
    extra = 1

class AgeRangeInline(admin.TabularInline):
    model = AgeRange
    extra = 1

class OfferAreaInline(admin.TabularInline):
    model = OfferArea
    extra = 1

class OfferAgeFilter(admin.SimpleListFilter):
    title = _('Age')
    parameter_name = 'age'

    def lookups(self, request, model_admin):
        AGE_RANGES = (
        ("1", "16-20"),
        ("2", "21-25"),
        ("3", "26-30"),
        ("4", "31-35"),
        ("5", "36-40"),
        ("6", "41-45"),
        ("7", "46-50"),
        ("8", "51-55"),
        ("9", "56-60"),
        ("10","60+"),
        )
        return AGE_RANGES

    def queryset(self, request, queryset):
        if (self.value() is None):
             return queryset
        else:
            return queryset.filter(agerange__age=self.value())

class OfferCountryFilter(admin.SimpleListFilter):
    title = _('Country')
    parameter_name = 'country'
    def lookups(self, request, model_admin):
        allcountry = set([o.country for o in OfferArea.objects.all()])
        return [(o.id, o.name) for o in allcountry]

    def queryset(self, request, queryset):
        if (self.value() is None):
             return queryset
        else:
            return queryset.filter(offerarea__country=self.value()).distinct()

class OfferCityFilter(admin.SimpleListFilter):
    title = _('City')
    parameter_name = 'city'
    def lookups(self, request, model_admin):
        allcity = set([o.city for o in OfferArea.objects.all()])
        ops = []
        for o in allcity:
            if (o is not None):
                ops.append((o.id, o.name))
        return ops

    def queryset(self, request, queryset):
        if (self.value() is None):
             return queryset
        else:
            return queryset.filter(offerarea__city=self.value()).distinct()

class OfferRegionFilter(admin.SimpleListFilter):
    title = _('Region')
    parameter_name = 'region'
    def lookups(self, request, model_admin):
        allregions = set([o.region for o in OfferArea.objects.all()])
        ops = []
        for o in allregions:
            if (o is not None):
                ops.append((o.id, o.name))
        return ops

    def queryset(self, request, queryset):
        if (self.value() is None):
             return queryset
        else:
            return queryset.filter(offerarea__region=self.value()).distinct()

class OfferAdmin(admin.ModelAdmin):
    search_fields = ('name', 'description')
    fieldsets = [
        (None, {'fields':['name', 'views', 'description', 'offer_type']}),
        ('Date information', {'fields':['start_date', 'end_date']}),
        ('Additional Information', {'fields':['gender', 'device_type', 'special_offer']}),
        ('Comments Status', {'fields':['comments_enabled']}),
    ]

    list_display = ('id', 'name', 'publish_date', 'start_date', 'end_date', 'special_offer', 'unique_views', 'views', 'comments',
                    'likes', 'comments_enabled', 'images_count', 'offer_type', 'gender',
                    'age_ranges', 'device_type', 'in_country', 'in_city', 'in_region')
    list_filter = ('gender', 'device_type', 'special_offer', OfferAgeFilter, OfferCountryFilter, OfferCityFilter, OfferRegionFilter)

    inlines = [AgeRangeInline, OfferAreaInline, OfferImageInline, CommentInline]

    """def change_view(self, request, object_id, form_url='', extra_context=None):
        from django.contrib.admin.utils import unquote
        obj = self.get_object(request, unquote(object_id))
        if obj:
            if obj.is_comments_enabled():
                self.inlines = [AgeRangeInline, OfferAreaInline, OfferImageInline, CommentInline]
            else:
                self.inlines = [AgeRangeInline, OfferAreaInline, OfferImageInline]
        return super(OfferAdmin, self).change_view(request, object_id, form_url, extra_context) """



    class Media:
        js = ['admin/js/foldable-list-filter.js']


class FiveYearListFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('Age Group')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'age_group'

    def lookups(self, request, model_admin):
        return (
            ('16-20', _('16-20')),
            ('21-25', _('21-25')),
            ('26-30', _('26-30')),
            ('31-35', _('31-35')),
            ('36-40', _('36-40')),
            ('41-45', _('41-45')),
            ('46-50', _('46-50')),
            ('51-55', _('51-55')),
            ('56-60', _('56-60')),
            ('60+', _('60+')),
        )

    def queryset(self, request, queryset):
        if self.value() == '60+':
            return queryset.filter(age__gte = 60)

        if self.value() == None:
            return queryset

        else :
            b = self.value().split('-')
            return queryset.filter(age__range=(b[0], b[1]))



class AppSettingAdmin(AdminViews):
    list_display = ('admob_ios', 'admob_android', 'admin_email')
    admin_views = (
                    ('Post Notifications', 'send_notification'),
        )

    def has_add_permission(self, request):
        return False if self.model.objects.count() > 0 else True

# Import-Export

from import_export.admin import ImportExportModelAdmin, ExportMixin

from import_export import resources, widgets, fields


class AppUserResource(resources.ModelResource):
    class Meta:
        model = AppUser

class UsersAdmin(ExportMixin, admin.ModelAdmin):
    search_fields = ('name', 'mobile')
    resource_class = AppUserResource
    list_display = ('name', 'age', 'gender', 'device_type', 'mobile', 'country', 'city', 'region')
    list_filter = ('age', 'gender', 'device_type', 'city', 'region')
    class Media:
        js = ['admin/js/foldable-list-filter.js']


admin.site.register(Offer, OfferAdmin)
admin.site.register(AppUser, UsersAdmin)
admin.site.register(AppSetting, AppSettingAdmin)

# from django.contrib.auth.models import User
# from django.contrib.auth.models import Group
# from push_notifications.models import APNSDevice, GCMDevice
#
# admin.site.unregister(User)
# admin.site.unregister(Group)
