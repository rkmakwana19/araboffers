from django.http import Http404

from offers.serializers import UserSerializer, UserWriterSerializer, OfferSerializer, CommentSerializer, OfferSerializerMany, CommentWriteSerializer, AppIdsSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from django.db.models import F
from django.shortcuts import render, redirect
from .forms import NotifForm
from django import forms

from django.db.models import Q
from django.utils import timezone
from django.core.paginator import Paginator

from .models import AppUser, Offer, Comment, AppSetting, OfferLike, OfferView
from areas.models import Country, City, Region
from push_notifications.models import APNSDevice, GCMDevice
import json
from django.http import HttpResponse
from clever_selects.views import ChainedSelectChoicesView

def notif(request):
    if request.method == 'GET':
        form = NotifForm()
    else:
        form = NotifForm(request.POST)
        if form.is_valid():
            message = request.POST.get('message', '')
            group = request.POST.get('group', '')
            age = request.POST.get('age', '')
            gender = request.POST.get('gender', '')
            offer_choice = request.POST.get('offer_choice', '')
            for_offer = request.POST.get('for_offer', '')
            country = request.POST.get('country', '')
            city = request.POST.get('city', '')
            region = request.POST.get('region', '')
            # print('PUSH NOTIFICATION SEND TO '+ message + ' ' + group + ' ' + age + ' ' + gender + ' ' + country + ' ' + city + ' ' + region )
            from django.contrib import messages
            if send_notif(message, group, age, gender, offer_choice, for_offer, country, city, region):
                messages.success(request, 'Notification Sent.')
                success = True
            else:
                success = False
                messages.success(request, 'Failed to send the notification.')
    return render(request, "notif_form.html", {'form': form})

def send_notif(msg, group, age, gender, offer_choice, for_offer, country, city, region):
    app_users = AppUser.objects.filter(Q(notif_all=True))
    if not age=="0":
        app_users = app_users.filter(Q(agerange__age=age))
    if not gender=="3":
        app_users = app_users.filter(Q(gender=gender))
    if country:
        app_users = app_users.filter(Q(country__id=int(country)))
    if city:
        app_users = app_users.filter(Q(city__id=int(city)))
    if region:
        app_users = app_users.filter(Q(region__id=int(region)))

    if offer_choice=='1':
        app_users = app_users.filter(Q(notif_supermarket=True))
    if offer_choice=='2':
        app_users = app_users.filter(Q(notif_restaurants=True))
    if offer_choice=='3':
        app_users = app_users.filter(Q(notif_collections=True))

    if group=='ios':
        devices = APNSDevice.objects.filter(name__in=[user.mobile for user in app_users])
        print('GAYA NOTIFICATIOM** *** * ** * * ** * * ** * **** *' + ' ' + str(devices.count()))
        devices.send_message(msg, badge=1, sound='default', extra={"event_id": for_offer})
        return True
    if group=='android':
        devices = GCMDevice.objects.filter(name__in=[user.mobile for user in app_users])
        print('GAYA NOTIFICATIOM** *** * ** * * ** * * ** * **** *' + ' ' + str(devices.count()))
        devices.send_message(msg, badge=1, sound='default', extra={"event_id": for_offer})
        return True
    if group=='all':
        devices = APNSDevice.objects.filter(name__in=[user.mobile for user in app_users])
        print('GAYA NOTIFICATION ** IOS ** *** * ** * * ** * * ** * **** *' + ' ' + str(devices.count()))
        devices.send_message(msg, badge=1, sound='default', extra={"event_id": for_offer})

        androidDevices = GCMDevice.objects.filter(name__in=[user.device_token for user in app_users])
        print('GAYA NOTIFICATION ** ANDROID ** *** * ** * * ** * * ** * **** *' + ' ' + str(androidDevices.count()))
        androidDevices.send_message(msg, badge=1, sound='default', extra={"event_id": for_offer})
        return True
    else:
        return False

class AjaxChainedCities(ChainedSelectChoicesView):
    def get_choices(self):
        print('GETTING ****** CITIES****** LIST ^^&&&#@&@&&&#&#&@&#&@')
        choices = []
        if self.parent_value == 0:
            return choices
        try:
            country = Country.objects.get(pk=self.parent_value)
            cities = City.objects.filter(country=country)
            allcities = set([o for o in cities])
            for city in cities:
                choices.append((city.id, city.name))
            return choices
        except KeyError:
            return []
        except Country.DoesNotExist:
            return []

class AjaxChainedRegions(ChainedSelectChoicesView):
    def get_choices(self):
        choices = []
        if self.parent_value == 0:
            return choices
        try:
            city = City.objects.get(pk=self.parent_value)
            regions = Region.objects.filter(city=city)
            allregions = set([o for o in regions])
            for region in regions:
                choices.append((region.id, region.name))
            return choices
        except KeyError:
            return []


class UserList(APIView):

    def get(self, request, format=None):
        users = AppUser.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = UserWriterSerializer(data=request.data)
        if serializer.is_valid():
            usr = None
            try:
                usr = AppUser.objects.get(mobile=request.data.get("mobile", None))
            except:
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            if usr:
                usr.delete()
                serializer.save()
                return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        user = self.get_object(pk)
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class UserDetail(APIView):
    """
    Retrieve, update or delete a user instance.
    """
    def get_object(self, pk):
        try:
            return AppUser.objects.get(pk=pk)
        except AppUser.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        user = self.get_object(pk)
        user = UserSerializer(user)
        return Response(user.data)

    def put(self, request, pk, format=None):
        user = self.get_object(pk)
        serializer = UserSerializer(user, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        user = self.get_object(pk)
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)




@api_view(['POST'])
def update_settings(request, pk):
    key = request.data.get("key", None)
    value = request.data.get("value", None)
    if int(value):
        value = True
    else:
        value = False
    try:
        user = AppUser.objects.get(pk=pk)
    except AppUser.DoesNotExist:
        raise Http404
    setattr(user, key, value)
    user.save()
    return Response(status.is_success(status.HTTP_200_OK))


class AppIdsDetails(APIView):
    def get(self, request, format=None):
        serializer = AppIdsSerializer(AppSetting.objects.all()[0], many=False)
        return Response(serializer.data)


class OfferList(APIView):

    def get(self, request, format=None):
        offers = Offer.objects.all()
        serializer = OfferSerializerMany(offers, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        print("POST OFFERS LIST REQUEST ***** ")
        user_id = request.POST.get("user_id", "")
        offer_category = request.POST.get("type", "")
        offset = int(request.POST.get("offset", ""))
	offset = offset+1
	limit=20

        if not user_id:
            today = timezone.now()
            offers = Offer.objects.all()
            offers = Offer.objects.filter(
                Q(end_date__gte=today),
                Q(start_date__lte=today),
                Q(offer_type=offer_category)
            ).order_by("-special_offer")
	    paginator = Paginator(offers, limit)
  	    off_page = paginator.page(offset)
	    offers = off_page.object_list
            serializer = OfferSerializerMany(offers, many=True)
            return Response(serializer.data)
        user = AppUser.objects.get(pk=user_id)
        region = user.region
        city = user.city
        country = user.country
        device = user.device_type
        age = user.age
        gender = user.gender
        today = timezone.now()
        offers = Offer.objects.all()
        offers = Offer.objects.filter(
            Q(end_date__gte=today),
            Q(start_date__lte=today),
            Q(device_type=device)|Q(device_type=None),
            Q(gender=gender)|Q(gender=None),
            Q(offer_type=offer_category),
            Q(agerange__age=age)|Q(agerange__age=None),
            Q(offerarea__country=country)|Q(offerarea__country=None),
            Q(offerarea__city=city)|Q(offerarea__city=None),
            Q(offerarea__region=region)|Q(offerarea__region=None)
        ).order_by("-special_offer")
	paginator = Paginator(offers, limit)
        off_page = paginator.page(offset)
        offers = off_page.object_list
        serializer = OfferSerializerMany(offers, many=True)
        return Response(serializer.data)

class OfferDetail(APIView):

    def get_object(self, pk):
        try:
            return Offer.objects.get(pk=pk)
        except Offer.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        offer = self.get_object(pk)
        offer = OfferSerializer(offer)
        return Response(offer.data)

    """ POST request for incrementing number of views on an offer """

    def post(self, request, pk, format=None):
        user = AppUser.objects.get(pk=request.POST.get("user_id", ""))
        offer = Offer.objects.filter(id=pk)
        offer.update(views=F('views')+1)

        view_exists = OfferView.objects.filter(user=user, offer=offer).exists()
        if (not view_exists):
            aview = OfferView(user=user, offer=offer[0])
            aview.save()

        if offer[0].offerlike_set.all().filter(user=user):
            offer = OfferSerializer(offer[0])
            d=offer.data
            d['i_like']=1
            return Response(d)
        else :
            offer = OfferSerializer(offer[0])
            d=offer.data
            d['i_like']=0
            return Response(d)

class LikeCreate(APIView):
    def post(self, request, format=None):
        user = AppUser.objects.get(pk=request.POST.get("user_id", ""))
        offer = Offer.objects.get(pk=request.POST.get("offer_id", ""))
        exists = OfferLike.objects.filter(user=user, offer=offer).exists()
        if exists:
            data_dict = {
                'success' : 1,
            }
            return Response(data_dict)
        else :
            like = OfferLike(user=user, offer=offer)
            like.save()
            data_dict = {
                'success' : 1,
            }
        return Response(data_dict)

class CommentDetail(APIView):
    """
    Retrieve, update or delete a comment instance.
    """

    def get(self, request, pk, format=None):
        comments = Comment.objects.filter(offer=Offer.objects.get(pk=pk))
        serializer = CommentSerializer(comments, many=True)
        return Response(serializer.data)


    def post(self, request, format=None):
        serializer = CommentWriteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def put(self, request, pk, format=None):
        user = self.get_object(pk)
        serializer = UserSerializer(user, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        user = self.get_object(pk)
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
