from .models import AppUser, Offer, Comment, AppSetting

from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):
    country = serializers.StringRelatedField(many=False)
    city = serializers.StringRelatedField(many=False)
    region = serializers.StringRelatedField(many=False)
    class Meta:
        model = AppUser
        fields = ('id', 'name', 'age', 'gender', 'mobile','country', 'city','region')

class UserWriterSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppUser
        fields = ('id', 'name', 'age', 'gender', 'mobile','country', 'city','region','device_token', 'device_type')

class CommentSerializer(serializers.ModelSerializer):
    user = UserSerializer(many=False)
    class Meta:
        model = Comment
        fields = ('id', 'text', 'time', 'user',)

class OfferSerializer(serializers.ModelSerializer):
    all_images = serializers.StringRelatedField(many=True)
    all_comments = CommentSerializer(many=True)
    class Meta:
        model = Offer
        fields = ('id', 'name', 'publish_date', 'start_date', 'end_date', 'views', 'description', 'all_images', 'all_comments', 'likes', 'special_offer', 'is_comments_enabled',)

class OfferSerializerMany(serializers.ModelSerializer):
    all_images = serializers.StringRelatedField(many=True)
    class Meta:
        model = Offer
        fields = ('id', 'name', 'publish_date', 'start_date', 'end_date', 'special_offer', 'views', 'description', 'all_images', 'comments', 'likes')



class CommentWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('id', 'text', 'time', 'user', 'offer')

class AppIdsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppSetting
        fields = ('admob_ios', 'admob_android', 'admin_email', 'share_message')
